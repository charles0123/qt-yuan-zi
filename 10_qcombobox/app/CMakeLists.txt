cmake_minimum_required(VERSION 3.5)

project(app LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

# QtCreator supports the following variables for Android, which are identical to qmake Android variables.
# Check http://doc.qt.io/qt-5/deployment-android.html for more information.
# They need to be set before the find_package(Qt5 ...) call.

#if(ANDROID)
#    set(ANDROID_PACKAGE_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/android")
#    if (ANDROID_ABI STREQUAL "armeabi-v7a")
#        set(ANDROID_EXTRA_LIBS
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libcrypto.so
#            ${CMAKE_CURRENT_SOURCE_DIR}/path/to/libssl.so)
#    endif()
#endif()

find_package(Qt5 COMPONENTS Widgets REQUIRED)
set(QRC_SOURCE_FILE ${CMAKE_CURRENT_SOURCE_DIR}/resources/resources.qrc)
qt5_add_resources(${QRC_SOURCE_FILE})

if(ANDROID)
  add_library(app SHARED
    main.cpp
    mainwindow.cpp
    mainwindow.h
    ${QRC_SOURCE_FILE}
  )
else()
  add_executable(app
    main.cpp
    mainwindow.cpp
    mainwindow.h
    ${QRC_SOURCE_FILE}
  )
endif()

target_link_libraries(app PRIVATE Qt5::Widgets)
