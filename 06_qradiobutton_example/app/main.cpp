#include "mainwindow.h"

#include <QApplication>
#include <QFile>

#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile file(":/qss/style.qss");

    if (file.exists())
    {
        cout << "find style" << endl;
        file.open(QFile::ReadOnly);
        QString styleSheet = QLatin1String(file.readAll());
        qApp->setStyleSheet(styleSheet);
        file.close();
    }

    MainWindow w;
    w.show();
    return a.exec();
}
