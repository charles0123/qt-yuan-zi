#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setGeometry(0,0,800,480);
    pushButton1 = new QPushButton("窗口皮肤1",this);
    pushButton2 = new QPushButton("窗口皮肤2",this);
    pushButton1->setGeometry(300,200,80,40);
    pushButton2->setGeometry(400,200,80,40);
    connect(pushButton1,SIGNAL(clicked()),this,SLOT(pushButton1_clicked()));
    connect(pushButton2,SIGNAL(clicked()),this,SLOT(pushButton2_clicked()));
}

MainWindow::~MainWindow()
{
}

void MainWindow::pushButton1_clicked()
{
    this->setStyleSheet("QMainWindow { background-color: rgba(255, 245, 238, 100%); }");
}

void MainWindow::pushButton2_clicked()
{
    this->setStyleSheet("QMainWindow { background-color: rgba(238, 122, 233, 100%); }");
}

