#include "mainwindow.h"
#include <QDesktopServices>
#include <QUrl>

using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    this->setGeometry(0, 0, 800, 480);
    this->setStyleSheet("QMainWindow {background-color: rgba(200, 50, 100, 100%);}");

    commandLinkButton = new QCommandLinkButton(this);

    commandLinkButton->setGeometry(350,200,250,50);
    commandLinkButton->setText("初始化为Checked状态");
    commandLinkButton->setChecked(false);
    commandLinkButton->setTristate();

    connect(commandLinkButton, SIGNAL(stateChanged(int)),this,SLOT(commandLinkButtonClicked()));
}

MainWindow::~MainWindow()
{
}

void MainWindow::commandLinkButtonClicked()
{

}

